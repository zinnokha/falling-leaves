# README #

Assignment Project for Front-end Developer Position at Truth Collective.

### Developer ###

* Zinno Kha 
* zinnokha@gmail.com
* (585)520-8939

### Details ###

* This site is hosted on a Linode.com server. NGINX PHP7.2 MariaDB
* Encryption provided by LetsEncrypt
* Basic HTML, SCSSPHP(leafo).
* Bootstrap4

Ideal Screen size / View port: 1300-1600px

https://fallingleaves.ivtecc.com

* Watch the Video & View All Features are functional!

### Core Code ###

Main Page
view-source:https://fallingleaves.ivtecc.com/

Raw SCSS
https://fallingleaves.ivtecc.com/assets/css/style.scss

Raw JS
https://fallingleaves.ivtecc.com/assets/js/page.js

Compiled CSS from Leafo
https://fallingleaves.ivtecc.com/assets/css/style.css

### To-do's ###

* Responsive!
* Video in Modal for Desktop view, Youtube app for mobile.
* View all features and Learn more should open up in a new page. Maybe a modal if content is limited.
* Slider for the screen shot details section.
* Parallax / Fade in some elements
* Contact us & Request a demo should open a Modal Form
* Sticky navigation for the different sections & main nav.
* SVG sprite library for icons.

