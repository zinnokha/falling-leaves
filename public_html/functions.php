<?php
include ("scssphp/scss.inc.php");
$scss = new scssc();
$scss_data = file_get_contents('assets/css/style.scss');
$scss_compiled = "/* Compiled " . date("Y-m-d H:i:s") . " */\n\n" . $scss->compile($scss_data);
file_put_contents('assets/css/style.css',$scss_compiled );
