<?php
include("functions.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Falling Leaves by Zinno Kha</title>
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<meta name="theme-color" content="#c1aee6">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/style.css">


</head>
<body>

<div id="section-hero">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center leaves" >
				<div class="leaf1" style="display: none;"><img src="assets/images/leaf1.png" alt=""></div>
				<div class="leaf2" style="display: none;"><img src="assets/images/leaf2.png" alt=""></div>
			</div>
			<div class="main-content col-12 text-center">
				<img src="assets/images/logo.png" alt="Falling Leaves Logo">
				<h1 class="color-white my-3">Falling Leaves</h1>
				<h2 class="color-white mb-5">This is free Application landing page for your business</h2>
				<p class="mb-4">
					<a href="#"><img class="px-md-1" src="assets/images/btn_download.png" alt="Download Falling Leaves"></a>
					<a href="#"><img class="px-md-1" src="assets/images/btn_buy.png" alt="Buy Falling Leaves"> </a>
				</p>
				<p><a class="video-btn" href="#" data-toggle="modal" data-src="https://www.youtube.com/embed/5s3HtiqTdI0" data-target="#video-modal"><img src="assets/images/btn_watch.png" alt="Watch Falling Leaves"></a></p>
			</div>
			<div class="col-12 text-center phone" >
				<div style="display: none;">
				<img class="img-fluid" src="assets/images/phone.png" alt="Watch Falling Leaves"> 
				</div>
			</div>
		</div>
	</div>
</div>

<div id="section-management">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1 class="mb-5">Best tool to manage your Diet</h1>
			</div>
			<div class="col-md-4 text-center mt-2 mb-4">
				<img src="assets/images/icon_management.png" alt="Easy management">
				<h2 class="my-3">Easy management</h2>
				<p>Ut placerat pharetra ipsum, in dignissim dui dapibus non. Suspendisse potenti. Phasellus volutpat nisi quis ipsum feugiat vulputate. </p>
			</div>
			<div class="col-md-4 text-center mt-2 mb-4">
				<img src="assets/images/icon_savetime.png" alt="Save your time">
				<h2 class="my-3">Save your time</h2>
				<p>Ut placerat pharetra ipsum, in dignissim dui dapibus non. Suspendisse potenti. Phasellus volutpat nisi quis ipsum feugiat vulputate. </p>
			</div>
			<div class="col-md-4 text-center mt-2 mb-4">
				<img src="assets/images/icon_savemoney.png" alt="Save your money">
				<h2 class="my-3">Save your money</h2>
				<p>Ut placerat pharetra ipsum, in dignissim dui dapibus non. Suspendisse potenti. Phasellus volutpat nisi quis ipsum feugiat vulputate. </p>
			</div>

			<div class="mt-3" style="min-height: 30px; width: 100%;">
				<div class="col-12 text-center">
					<p class="m-0">Interested in more? <a class="viewallfeatures-btn" href="#">View All Features</a></p>
				</div>
				
				<div class="viewallfeatures col-12" style="display: none;">
					<div class="row">
						<div class="col-md-4 text-center mt-2 mb-4">
							<img src="assets/images/icon_savemoney.png" alt="Save your money">
							<h2 class="my-3">Make everyone happy</h2>
							<p>Ut placerat pharetra ipsum, in dignissim dui dapibus non. Suspendisse potenti. Phasellus volutpat nisi quis ipsum feugiat vulputate. </p>
						</div>
						<div class="col-md-4 text-center mt-2 mb-4">
							<img src="assets/images/icon_management.png" alt="Easy management">
							<h2 class="my-3">More planning</h2>
							<p>Ut placerat pharetra ipsum, in dignissim dui dapibus non. Suspendisse potenti. Phasellus volutpat nisi quis ipsum feugiat vulputate. </p>
						</div>
						<div class="col-md-4 text-center mt-2 mb-4">
							<img src="assets/images/icon_savetime.png" alt="Save your time">
							<h2 class="my-3">Longer vacations!</h2>
							<p>Ut placerat pharetra ipsum, in dignissim dui dapibus non. Suspendisse potenti. Phasellus volutpat nisi quis ipsum feugiat vulputate. </p>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="section-managedash">
	<img class="huge-image-right img-fluid" src="assets/images/ipad.png" alt="Data Management via Dashboard">
	<div class="container">
		<div class="row py-5">
			<div class="col-md-6 my-5" >
				<h1 class="mb-3">Data Management via Dashboard</h1>
				<h2 class="mb-4">Lorem ipsum dolor sit amet, consectetur.</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer iaculis, lacus sed viverra interdum, nulla mi interdum eros, ac hendrerit urna lectus sit amet massa. Phasellus eget nisi eget lectus fringilla vehicula. Sed maximus velit ac lectus consequat condimentum. Etiam in accumsan quam.</p>
				<p class="mt-5"><a href="#"><img src="assets/images/btn_learnmore.png" alt="Learn more"></a>
			</div>
		</div>
	</div>
</div>

<div id="section-detail">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1 class="mb-3">Featured features</h1>
				<h2  class="mb-4">Phasellus eget nisi eget lectus fringilla vehicula</h2>
			</div>
			<div class="col-12 text-center mb-4">
				<img class="px-2" src="assets/images/pagination_default.png" alt="">
				<img class="px-2" src="assets/images/pagination_active.png" alt="">
				<img class="px-2" src="assets/images/pagination_default.png" alt="">
			</div>
			<div class="col-md-4 text-right ">
				<div class="mt-lg-5">
					<h2>Dashboard management</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer iaculis, lacus sed viverra interdum, nulla mi interdum eros, ac hendrerit.</p>
				</div>
				<div class="mt-lg-5">
					<h2>Exceptional Support</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer iaculis, lacus sed viverra interdum, nulla mi interdum eros, ac hendrerit.</p>
				</div>
				<div class="mt-lg-5">
					<h2>Instant Push Notifications</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer iaculis, lacus sed viverra interdum, nulla mi interdum eros, ac hendrerit.</p>
				</div>

			</div>
			<div class="col-md-4 text-center">
				<img class="huge-image-right img-fluid" src="assets/images/screenshot.png" alt="Falling Leaves features">
			</div>
			<div class="col-md-4 text-left">
				
				<div class="mt-lg-5">
					<h2>Sync data on cloud</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer iaculis, lacus sed viverra interdum, nulla mi interdum eros, ac hendrerit.</p>
				</div>
				<div class="mt-lg-5">
					<h2>Fast Delivery</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer iaculis, lacus sed viverra interdum, nulla mi interdum eros, ac hendrerit.</p>
				</div>
				<div class="mt-lg-5">
					<h2>Survive Surge Pricing</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer iaculis, lacus sed viverra interdum, nulla mi interdum eros, ac hendrerit.</p>
				</div>
			</div>

		</div>
	</div>
</div>


<div id="section-watch">
	
	<div class="container">
		<div class="row ">
			<div class="col-md-6 watch" >
				<div>
					<img class="img-fluid" src="assets/images/watch.png" alt="Data Management via Dashboard">
				</div>
			</div>
			<div class="watch-content col-md-6 text-center" >
				<h1 class="mb-3">Apps ready for Apple watch</h1>
				<h2 class="mb-4">Lorem ipsum dolor sit amet, consectetur.</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer iaculis, lacus sed viverra interdum, nulla mi interdum eros, ac hendrerit urna lectus sit amet massa. Phasellus eget nisi eget lectus fringilla vehicula. Sed maximus velit ac lectus consequat condimentum. Etiam in accumsan quam.</p>
				<p class="mt-5"><a href="#"><img src="assets/images/btn_appstore.png" alt="App Store"></a>
			</div>
		</div>
	</div>
</div>



<footer>
<div id="pre-footer">	
	<div class="container">
		<div class="row ">
			<div class="col-12 text-center" >
				<p class="m-0">Interested in seeing how awesome Falling Leaves can be for your business? Try it for FREE
				<a href="#"><img  class="pl-5" src="assets/images/btn_tryitfree.png" alt="tryitfree"></a>
				</p>
			</div>
		</div>
	</div>
</div>

<div id="footer">	
	<div class="container">
		<div class="row ">
			<div class="col-md-4 pr-5">
				<h3><img class="pr-2" src="assets/images/logo_white.png" alt="Falling Leaves Logo"> Falling Leaves</h3>
				<p class="pt-3">Ut placerat pharetra ipsum, in dignissim dui dapibus non. Suspendisse potenti. Phasellus volutpat nisi quis.</p>
			</div>
			<div class="col-md-2">
				<div class="link-block">
					<h4>ABOUT US</h4>
					<ul>
					<li><a href="#">Our team</a>
					<li><a href="#">Company</a>
					<li><a href="#">Jobs</a>
					<li><a href="#">Newsletters</a>
					</ul>
				</div>
			</div>
			<div class="col-md-2">
				<div class="link-block">
					<h4>HELP CENTER</h4>
					<ul>
					<li><a href="#">Documentations</a></li>
					<li><a href="#">Tutorials</a></li>
					<li><a href="#">Term of Use</a></li>
					<li><a href="#">Privacy Policy</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-2">
				<div class="link-block">
					<h4>TOOLS</h4>
					<ul>
					<li><a href="#">Create Account</a></li>
					<li><a href="#">Login</a></li>
					<li><a href="#">Services</a></li>
					<li><a href="#">Sitemap</a></li>
					</ul>

				</div>
			</div>
			<div class="col-md-2">
				<div class="link-block">
					<h4>GET IN TOUCH</h4>
					<ul>
					<li><a href="#">Contact us</a></li>
					<li><a href="#">Request a demo</a></li>
					<li><a href="#">+84 935-088-886</a></li>
					<li><a href="#">k3nnyart@gmail.com</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="post-footer">	
	<div class="container">
		<div class="row ">
			<div class="col-md-6 text-left" >
				<p class="m-0">© 2019, FallingLeaves, Inc. All Rights Reserved.</a></p>
			</div>
			<div class="col-md-6 text-right" >
				<p class="m-0"><a href="#">facebook</a>  -  <a href="#">twitter</a> - <a href="#">linkedin</a>  -  <a href="#">appstore</a></p>
			</div>
		</div>
	</div>
</div>


</footer>


<div class="modal fade" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
			<!-- 16:9 aspect ratio -->
			<div class="embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
			</div>
			</div>
		</div>
	</div>
</div> 


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/js/page.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	console.log('Loaded!');

	$('.leaf1').delay(1000).fadeIn();
	$('.leaf2').delay(1200).fadeIn();
	$('.phone div').delay(1400).fadeIn();

	//Viewallfeatures
	$('.viewallfeatures-btn').click(function(){
		$(this).parent().parent().fadeOut();
		$('.viewallfeatures').delay(1000).slideDown();
		return false;
	});


	// Video Modal
	var $videoSrc;  
	$('.video-btn').click(function() {
	$videoSrc = $(this).data( "src" );
	});
	console.log($videoSrc);
	$('#video-modal').on('shown.bs.modal', function (e) {
		$("#video").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
	});

	$('#video-modal').on('hide.bs.modal', function (e) {
		$("#video").attr('src',$videoSrc); 
	}); 

 
});
</script>
</body>
</html>